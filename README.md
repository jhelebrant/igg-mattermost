# igg-mattermost

> posts indiegogo campaign progress to a mattermost channel

![screenshot](screenshot.png)

## Dependencies

- Python >= 3.6
- requests
- pyaml
- pyppeteer

## Usage

First run:

```bash
virtualenv -p `which python3.6` .venv
source .venv/bin/activate
pip install -r requirements.txt
cp config.yml.example config.yml
# set campaign URL and Mattermost credentials in config.yml
python igg.py
# first run takes a while (pyppeteer downloads the chromium binaries)
```

## Notes

- does not work on RPi (pyppeteer downloads amd64/x86 chromium)
- don't run it too often, indiegogo has some pretty strict bot detection & rate limiting
