import asyncio
import pyppeteer
import requests
import json
import yaml
import sys

with open("config.yml", "r") as conf_file:
    config = yaml.load(conf_file)


async def main():
    browser = await pyppeteer.launch()
    page = await browser.newPage()
    await page.goto(config['URL'])
    try:
        data = await page.evaluate("""
        () => ({
            "title": document.querySelector('.campaignHeaderBasics-title').innerText,
            "raised": document.querySelector('.campaignGoalProgress-raised').innerText,
            "funded": document.querySelector('[gogo-test="percent_funded"]').innerText,
            "percents": parseFloat(document.querySelector('[gogo-test="percent_funded"] em').innerText),
            "time_left": document.querySelector('.campaignGoalProgress-detailsTimeLeft').innerText
        })
        """)
    except pyppeteer.errors.ElementHandleError:
        sys.stderr.write("Looks like we've hit Indiegogo's bot detection / rate limiting.\n")
    else:
        bar_conf = {
            "empty": "░",
            "full": "▓",
            "size": int(config["BAR_SIZE"])
        }
        num_filled = int(data["percents"] / 100 * bar_conf["size"])
        bars_full = bar_conf["full"] * num_filled
        bars_empty = bar_conf["empty"] * (bar_conf["size"] - num_filled)
        bar = f"{bars_full}{bars_empty}"
        message = f"**{data['title']}**\n**{data['raised']}**\n{bar}\n{data['funded']}— {data['time_left']}"
        payload = json.dumps({
            "channel": config["MATTERMOST_CHANNEL"],
            "username": config["MATTERMOST_USERNAME"],
            "text": message
        })
        requests.post(config["MATTERMOST_WEBHOOK"], data=payload, timeout=config["HTTP_TIMEOUT"])
    await browser.close()

asyncio.get_event_loop().run_until_complete(main())
